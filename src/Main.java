import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Train[] trains = new Train[5];

        trains[0] = new Train("Baku", 1, 1400);
        trains[1] = new Train("Yerevan", 3, 2300);
        trains[2] = new Train("Chattanooga", 4, 1200);
        trains[3] = new Train("Yerevan", 2, 2100);
        trains[4] = new Train("Baku", 5, 1130);


        System.out.println(Train.displayInformation(trains,input()));


        Train[] arraySortedByNumbers = Train.sortingTrainsByTheirNumbers(trains);
        System.out.println(Arrays.toString(arraySortedByNumbers));


        Train[] arraySortedByDestination = Train.sortingTrainsByTheirDestination(trains);
        System.out.println(Arrays.toString(arraySortedByDestination));

    }

    public static int input() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}