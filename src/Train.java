public class Train {
    private String destinationName;
    private int trainNumber;
    private int departureTime;

    public Train(String destinationName, int trainNumber, int departureTime){
        this.destinationName = destinationName;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
    }



    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public int getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(int departureTime) {
        this.departureTime = departureTime;
    }

    public String toString(){
        return this.destinationName + " " + this.trainNumber + " " + this.departureTime;
    }


//Method for displaying information for one selected train.
// As arguments, it uses the array of trains and inputted number
// Input method itself was written in MAIN class
    public static String displayInformation(Train[] trains, int number){
        int inputNumber = number;

        for(Train train : trains ){
            if (inputNumber == train.getTrainNumber()){
                return train.getTrainNumber() + " " + train.getDestinationName()
                        + " " + train.getDepartureTime();
            }
        }
        return "0";
    }


// Method for sorting array of trains by their numbers
    public static Train[] sortingTrainsByTheirNumbers(Train[] trains) {
        for (int i = 0; i < trains.length; i++) {
            int pos = i;
            for (int j = i + 1; j < trains.length; j++) {
                if (trains[j].getTrainNumber() < trains[pos].getTrainNumber()) {
                    pos = j;
                }
            }
            Train temp = trains[pos];
            trains[pos] = trains[i];
            trains[i] = temp;
        }
        return trains;
    }


// Method to sort array of Trains by their destination AND departure time
// It was implemented as parts of one IF\IF-ELSE cycle
    public static Train[] sortingTrainsByTheirDestination(Train[] trains) {
        for (int i = 0; i < trains.length; i++) {
            for (int j = i + 1; j < trains.length; j++) {
                if (trains[i].getDestinationName() == trains[j].getDestinationName()
                        && trains[i].getDepartureTime() > trains[j].getDepartureTime()){

                    Train temp = trains[j];
                    trains[j] = trains[i];
                    trains[i] = temp;

                }
                else if (trains[i].getDestinationName().compareTo(trains[j].getDestinationName()) > 0) {
                    Train temp = trains[i];
                    trains[i] = trains[j];
                    trains[j] = temp;
                }
            }
        }return trains;
    }


}
